#include "myShortestPath.h"

struct vertex_dijkstra
{
  Vertex vertex;
  int dist;
  Vertex pred;
  bool min_dist = false;
  bool is_scanned = false;
  std::vector<Edge> delta;
};


bool sort_dist(const vertex_dijkstra &v1, const vertex_dijkstra &v2){
    return(v1.dist < v2.dist);
}

std::vector<Vertex> my_shortest_path(const Graph &g, const Vertex &startVertex,
                                     const Vertex &endVertex){
  
  auto weight_map = boost::get(boost::edge_weight,g);
  std::vector<Vertex> path;
  std::vector<vertex_dijkstra> all_v;
  std::vector<vertex_dijkstra> notscan_and_finite;

  auto it_vertex = boost::vertices(g);
  // Step 1
  for (auto it = it_vertex.first; it != it_vertex.second; it++)
  {  
    if (*it==startVertex){
      struct vertex_dijkstra dv; 
      dv.vertex = *it;
      dv.dist = 0; 
      dv.pred = *it;
      dv.min_dist = true;
      dv.is_scanned = true;
      auto it_edge = boost::out_edges(*it,g);
      for (auto iti = it_edge.first; iti != it_edge.second; iti++){
        dv.delta.push_back(*iti);
      }
      all_v.push_back(dv);
      notscan_and_finite.push_back(dv);
    }else{
      struct vertex_dijkstra dv; 
      dv.vertex = *it;
      dv.dist = 100000; 
      dv.pred = *it;
      auto it_edge = boost::out_edges(*it,g);
      for (auto iti = it_edge.first; iti != it_edge.second; iti++){
        dv.delta.push_back(*iti);
      }
      all_v.push_back(dv);
    }
  }
  // Step 2
  
  while(notscan_and_finite.size() != 0){
    std::vector<vertex_dijkstra> notscan_and_finite_temp;
      std::sort(notscan_and_finite.begin(),notscan_and_finite.end(),sort_dist); 
      vertex_dijkstra current_vertex = notscan_and_finite[0];
        for (size_t j = 0; j < current_vertex.delta.size(); j++){
            auto index = boost::target(current_vertex.delta[j],g); 
            vertex_dijkstra target = all_v[index];
            auto weight = weight_map[current_vertex.delta[j]]; 
            auto triangle_leftsite  = current_vertex.dist + weight; 
            auto triangle_rightsite = target.dist;
            if(triangle_leftsite < triangle_rightsite){
              target.dist = triangle_leftsite;
              target.pred = current_vertex.vertex;
              notscan_and_finite_temp.push_back(target);
              all_v[index]=target;
            }
        }
    notscan_and_finite.erase(notscan_and_finite.begin());
    for( int k = 0; k < notscan_and_finite_temp.size(); k++){
      notscan_and_finite.push_back(notscan_and_finite_temp[k]);
    }
}

  Vertex index_vert = endVertex;
  path.push_back(endVertex);
  while (index_vert != startVertex){
    path.push_back(all_v[index_vert].pred);
    index_vert = all_v[index_vert].pred;
  }
  std::reverse(path.begin(),path.end());
  return path;
}
