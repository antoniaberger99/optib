#include "mySpanningTree.h"

//sorting edges depending on their cost
bool sort_edges(const Edge &e1, const Edge&e2){
  
  EdgeWeightProperty* e_weight_1 = (EdgeWeightProperty*)e1.get_property();
  unsigned int weight_1 = (*e_weight_1).m_value; 
  EdgeWeightProperty* e_weight_2 = (EdgeWeightProperty*)e2.get_property();
  unsigned int weight_2 = (*e_weight_2).m_value; 

  return(weight_1 < weight_2);
}

//for testing purposes
void print(std::vector<Edge> &v){
for(auto e: v){
  EdgeWeightProperty* e_weight = (EdgeWeightProperty*)e.get_property();
  unsigned int weight = (*e_weight).m_value;
  printf("(%zu)-- %u -> (%zu)\n,",e.m_source,weight, e.m_target);
  }
}

//checking if adding edge e to the spanning tree would result in a circle
bool no_cycle(const Graph &g, Edge &e, std::vector<int>& labels){
  
  size_t tag = boost::target(e, g);
  size_t sour = boost::source(e, g);

  if (labels[tag] == labels[sour]) {
      return false;
  }
  else {
      int labelSource = labels[sour];
      for (int i = 0; i < labels.size(); i++) {
          if (labels[i] == labelSource) {
              labels[i] = labels[tag];
          }
      }
      return true;
  }

}

std::vector<Edge> my_spanning_tree(const Graph &g) {
  std::vector<Edge> spanning_tree;
  //using Kruskals algorithm
  
  //gather all edges and sort them dependent on their cost
  auto it_edges = boost::edges(g);
  std::vector<Edge> all_e;
  for (auto it = it_edges.first; it != it_edges.second; it++){
   all_e.push_back(*it);
  }
  std::sort(all_e.begin(),all_e.end(),sort_edges);

  //initialize labels for efficient implementation
  std::vector<int> labels(g.m_vertices.size());
  for (int i = 0; i < labels.size(); i++) {
      labels[i] = i;
  }

  //add edges successively to the spanning tree
  int num_v = boost::num_vertices(g);
  int ed = 0;
  for (auto i = 0; i < all_e.size() && ed <= num_v - 1; i ++){
    if (no_cycle(g,all_e[i], labels)){
      spanning_tree.push_back(all_e[i]);
      ed = ed + 1;
    }
  }

  return spanning_tree;
}