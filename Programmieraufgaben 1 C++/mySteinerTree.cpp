#include "mySteinerTree.h"

#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>


std::vector<Edge> my_steiner_tree(const Graph &g,
                                  const std::vector<Vertex> &terminals) {
  std::vector<Edge> steiner_tree;
  std::map<Vertex, Vertex> t_inverse;
  std::map<Edge, std::vector<Edge>> r_edges;
  Graph red_graph;

  for (auto n : terminals){
    auto vert = boost::add_vertex(red_graph);
    t_inverse.insert(std::pair<Vertex, Vertex>(n, vert));
  }

  for (auto start_vertex : terminals){
    std::vector<Vertex> pred(num_vertices(g));
    std::vector<int> dist(num_vertices(g));
    boost::dijkstra_shortest_paths(g, start_vertex, boost::predecessor_map(&pred[0]).distance_map(&dist[0]));

    for (auto target_vertex : terminals){
      if (start_vertex != target_vertex){
        auto added_edge = boost::add_edge(t_inverse[start_vertex], 
        t_inverse[target_vertex],EdgeWeightProperty(dist[target_vertex]), red_graph);

        std::vector<Edge> path;
        Vertex walker = target_vertex;
        while (walker != start_vertex){
          Edge e = boost::edge(walker, pred[walker], g).first;
          path.push_back(e);
          walker = pred[walker];
        }
        r_edges.insert(std::pair<Edge, std::vector<Edge>>(added_edge.first, path));
      }
    }
  }
  std::vector<Edge> mst_reduced;
  boost::kruskal_minimum_spanning_tree(red_graph, std::back_inserter(mst_reduced));

  for (auto pseudo_edge : mst_reduced){
    for(auto edge: r_edges[pseudo_edge]){
      steiner_tree.push_back(edge);
    }
  }

  return steiner_tree;
}
