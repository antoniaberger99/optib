#include "MyFootballBettingGame.h"
#include <boost/graph/maximum_weighted_matching.hpp>
#include "types.h"
#include <iostream>
#include <boost/graph/graphviz.hpp>
#include <fstream>

std::map<Team, unsigned int> my_football_betting(const std::vector<Matchday> &season)
{
  std::map<Team, DiVertex> v_team;
  std::map<Matchday, DiVertex> day_vertices;

  std::map<Team, unsigned int> myBet;

  DiGraph g;
  auto v_name_map = boost::get(boost::vertex_name, g);
  EdgeWeightMap edge_weight_map = boost::get(boost::edge_weight, g);

  // jedes Team erhällt einen Konten
  Matchday md1 = season[0];
  for (Match m : md1){
    auto v_team1 = boost::add_vertex(g);
    auto v_team2 = boost::add_vertex(g);
    v_team.insert(std::pair<Team, size_t>{m.first.first, v_team1});
    v_team.insert(std::pair<Team, size_t>{m.first.second, v_team2});

    v_name_map[v_team1] = m.first.first;
    v_name_map[v_team2] = m.first.second;
  }

  // jeder Matchday ist ein Knoten
  for (Matchday day : season){
    DiVertex v_day = boost::add_vertex(g);
    day_vertices.insert(std::pair<Matchday, DiVertex>{day, v_day});
    for (Match match : day){
      //Get team vertices for both teams
      DiVertex v_team1 = v_team[match.first.first];
      DiVertex v_team2 = v_team[match.first.second];

      //Create arcs
      Arc arc_team1 = boost::add_edge(v_day, v_team1, g).first;
      Arc arc_team2 = boost::add_edge(v_day, v_team2, g).first;

      // Verteile Punkte nach Spielstand
      Score score = match.second;
      if (score.first > score.second){
        //Team 1 gewinnt
        edge_weight_map[arc_team1] = 3;
        edge_weight_map[arc_team2] = 0;
      }
      else if (score.second > score.first){
        //Team 2 gewinnt
        edge_weight_map[arc_team1] = 0;
        edge_weight_map[arc_team2] = 3;
      }
      else{
        // Unendschieden
        edge_weight_map[arc_team1] = 1;
        edge_weight_map[arc_team2] = 1;
      }
    }
  }

  std::ofstream hallo; 
  hallo.open("test.dot");
  boost::write_graphviz(hallo,g);

  DiVertexIterator vit, vend;
  ArcIterator ait, aend;
  
  for (boost::tie(vit, vend) = boost::vertices(g); vit != vend; vit++){
    std::cout << "v:\t" << *vit << std::endl;
  }

  for (boost::tie(ait, aend) = boost::edges(g); ait != aend; ait++){
    std::cout << "e:\t" << *ait <<  "|" << edge_weight_map[*ait] <<std::endl;
  }

  // max cost matching
  std::vector<DiVertex> matching(num_vertices(g));
  boost::maximum_weighted_matching(g, &matching[0]);
  for (DiVertex m : matching){
    std::cout << "Opt Mate for " << m << " is " << matching[m] << std::endl;
  }

  unsigned int day_num = 0;
  for (Matchday m : season){
    myBet.insert(std::pair<std::string, unsigned int>{v_name_map[matching[day_vertices[m]]], day_num});
  }

  return myBet;
}
