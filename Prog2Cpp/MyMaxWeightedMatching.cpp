#include <iostream>
//#include <boost/graph/graphviz.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <fstream>
#include "MyMaxWeightedMatching.h"
#include <boost/graph/bipartite.hpp>
typedef std::vector<boost::default_color_type> partition_t;
typedef boost::property_map<DiGraph, boost::vertex_index_t>::type index_map_t;
typedef boost::iterator_property_map<partition_t::iterator, index_map_t> partition_map_t;
typedef boost::property_map<DiGraph, boost::vertex_color_t>::type color_map_t;

std::vector<Vertex> my_max_weighted_matching(const Graph &g){

  std::vector<Vertex> mate(num_vertices(g));
  DiGraph gP;

  auto g_verti = boost::vertices(g);
  auto g_edg = boost::edges(g);

  // Zugriff auf G'
  auto gP_weight_map = boost::get(boost::edge_weight, gP);
  auto gP_capacity_map = boost::get(boost::edge_capacity, gP);
  auto g_prime_color_map = boost::get(boost::vertex_color, gP);

  // Zugriff auf G
  auto weight_map_g = boost::get(boost::edge_weight, g);

  // G' generieren
  //schreibe alle knoten von g in G'
  for (auto i = g_verti.first; i != g_verti.second; i++){
    boost::add_vertex(gP);
  }

  //schreibe alle Kanten von g in G'
  for (auto i = g_edg.first; i != g_edg.second; i++){
    Vertex s = boost::source(*i, g);
    Vertex t = boost::target(*i, g);
    Arc a = boost::add_edge(s, t, gP).first;

  unsigned int max_c = 0; 
  BGL_FORALL_EDGES_T(e, g, Graph){
    if (weight_map_g[e] > max_c){
      max_c = weight_map_g[e];  
    }
  }
    //Funktionen auf Kanten (Gewichtsfunktion,Kapazitätsfunktion const 1 (um Matching zu gewährleisten))
    // c'(e) in G':  max(c)- c(e) in g.
    gP_weight_map[a] = max_c - weight_map_g[*i];  
    gP_capacity_map[a] = 1;                      

  }

  // Berechne Partion des Bipartieten Graphen (haben Aufgabe 2 nicht gemacht)
  partition_t g_prime_partition(boost::num_vertices(gP));
  partition_map_t g_prime_partition_map(g_prime_partition.begin(), boost::get(boost::vertex_index, gP));
  bool is_bipartite = boost::is_bipartite(g, boost::get(boost::vertex_index, gP), g_prime_partition_map);


  // Grapf funktioniert nur bei Biparteiren graphen, deswegen:  
  if (!is_bipartite)
    return mate;

  //Fünge "dumminodes" hinzu 
  DiVertex s = boost::add_vertex(gP); 
  DiVertex t = boost::add_vertex(gP); 
  g_prime_color_map[s] = boost::default_color_type::green_color;
  g_prime_color_map[t] = boost::default_color_type::green_color;


  DiVertexIterator it, end_it;
  for (boost::tie(it, end_it) = boost::vertices(gP); it != end_it; it++){
    if (*it == s || *it == t) // brauche mit s und t nicht anzugucken
      continue;

    // verbinde s mit allen Knoten in A und t mit allen in B
    auto color = boost::get(g_prime_partition_map, *it); 
    g_prime_color_map[*it] = color;                    

    if (color == boost::default_color_type::white_color){
      Arc a = boost::add_edge(s, *it, gP).first;
      gP_capacity_map[a] = 1;
      gP_weight_map[a] = 0;

    }
    else if (color == boost::default_color_type::black_color){
      Arc a = boost::add_edge(*it, t, gP).first;
      gP_capacity_map[a] = 1;
      gP_weight_map[a] = 0;
    }
  }

  // Min cost Fluss auf G'
  //  Augment graph to G_aug
  //  Calculate min cost flow in G_aug.

  // agumentierter graph gA mit zugriffen.
  DiGraph gA;
  auto gA_weight_map = boost::get(boost::edge_weight, gA);
  auto gA_capacity_map = boost::get(boost::edge_capacity, gA);
  auto gA_residual_cap_map = boost::get(boost::edge_residual_capacity, gA);
  auto gA_reverse_map = boost::get(boost::edge_reverse, gA);
  auto gA_color_map = boost::get(boost::vertex_color, gA);

  BGL_FORALL_VERTICES_T(v, gP, DiGraph){
    DiVertex added = boost::add_vertex(gA);
    gA_color_map[added] = g_prime_color_map[v];
  }
  // erstelle gA
  BGL_FORALL_EDGES_T(e, gP, DiGraph){
    DiVertex source = boost::source(e, gP);
    DiVertex target = boost::target(e, gP);
    Arc orig = boost::add_edge(source, target, gA).first;
    Arc reverse = boost::add_edge(target, source, gA).first;
    gA_weight_map[orig] = gP_weight_map[e];
    gA_capacity_map[orig] = gP_capacity_map[e];
    gA_weight_map[reverse] = -1 * gP_weight_map[e];
    gA_capacity_map[reverse] = 0;
    gA_reverse_map[orig] = reverse;
    gA_reverse_map[reverse] = orig;
  }

  boost::successive_shortest_path_nonnegative_weights(gA, s, t);

  // erstelle den Vektor mate 
  BGL_FORALL_EDGES_T(e, gA, DiGraph){
    DiVertex s = boost::source(e, gA);
    DiVertex t = boost::target(e, gA);
    if (gA_color_map[s] == boost::default_color_type::white_color){
      if (gA_residual_cap_map[e] == 0){
        mate[s] = t;
        mate[t] = s;
      }
    }
  }
  return mate;
}
